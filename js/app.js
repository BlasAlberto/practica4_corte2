//Primera función
function valorPromedio(vector){
    let promedio = 0.0;
    let tamaño = vector.length;

    if(tamaño > 1){
        for(let i=0; i<tamaño; promedio += vector[i], i++);
        promedio = promedio/tamaño;
    }

    return promedio;
}

//Segunda función
function cantidadPares(vector){
    let cantidad = 0;
    let tamaño = vector.length;
    
    if(tamaño > 1)
        for(let i=0; i<tamaño; i++)
            if(vector[i]%2 == 0) cantidad++;

    return cantidad;
}

//Tercera función
function ordenDescendente(arreglo){
    let orden = new Array();
    orden = arreglo.sort(function(a, b){return b - a})
    return orden;
}
//Tercera función
function ordenAscendente(arreglo){
    let orden = new Array();
    orden = arreglo.sort(function(a, b){return a - b})
    return orden;
}



//Testeo
var arreglo = [
    20, 25, 19, 30, 52,
    30, 26, 23, 25, 18,
    15, 12, 28, 48, 23,
    22, 21, 28, 27, 24];

console.log("Resultado 1: " + valorPromedio(arreglo));
console.log("Resultado 2: " + cantidadPares(arreglo));
console.log("Resultado 3: " + ordenDescendente(arreglo));



//Función - Devolver la cantidad de numeros pares en un vector
function contarPares(vector){
    let cantidad = 0;

    for(let i=0; i<vector.length; i++){
        if(vector[i]%2 == 0) cantidad++;
    }

    return cantidad;
}



//Función - Generar numeros aleatorios del 1 al 50
function llenar(){
    var limite = document.getElementById('limite').value;

    if(limite != ""){
        var listaNumeros = document.getElementById('numeros');
        var listaAscendente = new Array(limite);
        
        //Generar numeros y ordenarlos
        for(let con=0;
            con<limite;
            listaAscendente[con]=Math.floor(Math.random()*50)+1,
            con++);
        listaAscendente = ordenAscendente(listaAscendente);
        
        //Agregar numeros al componente Select
        for(let con=0; 
            con<limite; 
            listaNumeros.options[con] = new Option(listaAscendente[con], 'valor:' + listaAscendente[con]), 
            con++);

        //Mostrar componentes ocultos
        document.getElementById('lblNumeros').style = 'display: block';
        listaNumeros.style = 'display: block';
        document.getElementById('divPorcentajes').style = 'display: block';


        //Mostrar información sobre la simetria
        let lblPares = document.getElementById('porPares');
        let lblImpares = document.getElementById('porImpares');
        let lblSimetria = document.getElementById('EsSimetrico');
        let cantPares = cantidadPares(listaAscendente);

        lblPares.innerHTML = ((cantPares/limite)*100).toFixed(2) + "%";
        lblImpares.innerHTML = (((limite-cantPares)/limite)*100).toFixed(2) + "%";
        
        if(cantPares/limite >= 0.375 && cantPares/limite <= 0.625){
            lblSimetria.style = "color: #375047";
            lblSimetria.innerHTML = "Es simetrico";
        }
        else{
            lblSimetria.style = "color: brown";
            lblSimetria.innerHTML = "No es simetrico";
        }
    }
    else{
        alert("Ingrese una cantidad");
    }
}



//Validación para puros numeros enteros y backspace
function valideKey(evt){
    
    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}